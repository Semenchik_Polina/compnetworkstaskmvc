﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CompNetworksTaskMVC.Controllers.Utils
{
    public static class NetworkUtil
    {
        public static string GetUrl(Controller controller)
        {
            string baseUrl = $"{controller.Request.Scheme}://{controller.Request.Host}{controller.Request.PathBase}";
            return baseUrl;
        }

        public static string GetIP()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName()); // `Dns.Resolve()` method is deprecated.
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            string ip = ipAddress.ToString();
            return ip;
        }
    }
}
